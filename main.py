import discord
import os
import logging
import traceback

from datetime import datetime
from discord.ext import commands
778888888
from remodel.connection import pool
from remodel.helpers import create_tables, create_indexes
from remodel.models import Model

from cogs.permissions import Permission
from cogs.self_assign_roles import SelfAssignRole

client = discord.Client()
TOKEN = os.getenv('DISCORD_BOT_INITIATIVE_TOKEN')
PREFIX = os.getenv('DISCORD_BOT_INITIATIVE_PREFIX')


class CoreConfig(Model):
    has_one = ('Extensions',)


class Extensions(Model):
    pass


class InitiativeBot(commands.bot):
    def __init__(self):
        self.uptime = datetime.utcnow()

        pool.configure(max_connections=4, host="localhost", port=28015, user="admin", password="",
                       db="discord_bot_initiative")

        create_tables()
        create_indexes()

        if PREFIX is not None:
            CoreConfig["prefix"] = PREFIX

        bot = commands.Bot(command_prefix=CoreConfig["prefix"],
                           description="Initiative is a general purpose bot for the EncounterVTT Discord")

        @bot.check
        async def globally_block_dms(ctx):
            return ctx.guild is not None

        bot.load_extension("cogs.permissions")
        bot.load_extension("cogs.self_assign_roles")
        bot.load_extension("cogs.announce")

