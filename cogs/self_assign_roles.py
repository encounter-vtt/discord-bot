from typing import Optional

import discord
from discord.ext import commands
from discord import Client, Reaction, Member, PartialEmoji, Message, Role

from remodel.models import Model

from cogs.permissions import has_perm


class SelfAssignableRole(Model):
    pass


class SelfAssignRole(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.group()
    @has_perm("admin")
    async def role(self, ctx, *args):
        pass

    @role.command()
    async def add(self, ctx: commands.Context,
                  role:    Role,
                  message: Message,
                  emote:   PartialEmoji):
        SelfAssignableRole.create(role_id=role.id, message_id=message.id, emote_id=emote.id)
        await ctx.send("Self-assignable Role created")

    @role.command()
    async def rem(self, ctx: commands.Context,
                  role:    Optional[Role],
                  message: Optional[Message],
                  emote:   Optional[PartialEmoji]):
        query = {}

        if role is not None:
            query['role_id'] = role.id

        if message is not None:
            query['message_id'] = message.id

        if emote is not None:
            query['emote_id'] = emote.id

        count = 0
        for r in SelfAssignableRole.filter(**query):
            count += 1
            r.delete()

        if count is 0:
            await ctx.send("No matches found")
        elif count is 1:
            await ctx.send("Deleted 1 match")
        else:
            await ctx.send(f"Deleted {count} matches")

    @commands.Cog.listener()
    async def on_reaction_add(self, reaction: Reaction, user):
        if isinstance(user, Member):
            role = SelfAssignableRole.get(message_id=reaction.message.id, emote_id=reaction.emoji.id)
            if role is not None:
                d_role = user.guild.get_role(role['role_id'])
                if not any(r.id == d_role.id for r in user.roles):
                    await user.add_roles(d_role, reason="Self-assigned")

    @commands.Cog.listener()
    async def on_reaction_remove(self, reaction: Reaction, user):
        if isinstance(user, Member):
            role = SelfAssignableRole.get(message_id=reaction.message.id, emote_id=reaction.emoji.id)
            if role is not None:
                d_role = user.guild.get_role(role['role_id'])
                if any(r.id == d_role.id for r in user.roles):
                    await user.remove_roles(d_role, reason="Self-removed")
