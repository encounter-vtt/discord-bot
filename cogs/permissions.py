import os

from discord import Member, Role
from discord.ext import commands
from discord.ext.commands import Context, Cog, Bot
from remodel.models import Model
from remodel.object_handler import ObjectHandler


class PermissionObjectHandler(ObjectHandler):
    def verify(self, user: Member, perm: str):
        return len(self.filter(user_id=user.id, permission=perm).iterator()) > 0 \
               or any(any(perm['role_id'] == r.id for perm in self.all()) for r in user.roles)


class Permission(Model):
    object_handler = PermissionObjectHandler


def has_perm(perm: str):
    def predicate(ctx: Context):
        return Permission.verify(ctx.author, perm)
    return commands.check(predicate)


class PermissionsCog(Cog):
    rule_exists_msg = "Permission rule already exists"
    rule_doesnt_exist_mst = "Permission rule doesn't exist"

    def __init__(self, bot):
        self.bot = bot

    def cog_check(self, ctx: Context):
        return Permission.verify(ctx.author, "admin")

    @commands.group()
    async def add(self, ctx, *args):
        pass

    @commands.group()
    async def rem(self, ctx, *args):
        pass

    @add.command("role")
    async def add_role(self, ctx: Context, role: Role, perm: str):
        if Permission.get(role_id=role.id, permission=perm) is None:
            Permission.create(role_id=role.id, permission=perm)
            await ctx.message.add_reaction('👍')
        else:
            await ctx.send(self.rule_exists_msg)

    @add.command("user")
    async def add_user(self, ctx: Context, user: Member, perm: str):
        if Permission.get(user_id=user.id, permission=perm) is None:
            Permission.create(user_id=user.id, permission=perm)
            await ctx.message.add_reaction('👍')
        else:
            await ctx.send(self.rule_exists_msg)

    @rem.command("role")
    async def rem_role(self, ctx: Context, role: Role, perm: str):
        rule = Permission.get(role_id=role.id, permission=perm)
        if rule is not None:
            rule.delete()
            await ctx.message.add_reaction('👍')
        else:
            await ctx.send(self.rule_doesnt_exist_mst)

    @rem.command("user")
    async def rem_user(self, ctx: Context, user: Member, perm: str):
        rule = Permission.get(user_id=user.id, permission=perm)
        if rule is not None:
            rule.delete()
            await ctx.message.add_reaction('👍')
        else:
            await ctx.send(self.rule_doesnt_exist_mst)


def setup(bot: Bot):
    bot.add_cog(PermissionsCog)

    admin = os.getenv('DISCORD_BOT_INITIATIVE_ADMIN_ROLE')
    if admin is not None:
        Permission.create(role_id=int(admin), perm="admin")

